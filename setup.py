# -*- coding: utf-8 -*-
import imp
import os

from setuptools import setup

import subprocess


def read(fname):
    u"""Чтение файла."""
    try:
        return open(os.path.join(os.path.dirname(__file__), fname)).read()
    except IOError:
        return ''


def _load_requirements(filename):
    u"""Загрузка списка зависимостей из файла ``filename``."""
    file_path = os.path.join(
        os.path.dirname(__file__),
        filename,
    )
    result = []
    dep_links = []
    with open(file_path, 'r') as f:
        for line in f:
            line = line.strip()
            if line.startswith('-e'):
                dep_links.append(line)
            elif not line.startswith('#') and not line.startswith('-r'):
                result.append(line)

    return result, dep_links


requirements, dep_links = _load_requirements('requirements.txt')


setup(
    name='m3-gar-client',
    version='0.0.1',
    description=read('DESCRIPTION'),
    license="GPL",
    keywords="django m3",

    url="http://bars-czm.ru/",
    packages=['m3_gar_client'],
    package_dir={'m3_gar_client': 'src/m3_gar_client'},
    include_package_data=True,
    zip_safe=False,
    long_description=read('README'),
    install_requires=requirements,
    cmdclass={
    },
)
